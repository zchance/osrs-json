package zchance;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.*;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Pulls data from the Grand Exchange
 *
 * @author Zed Chance
 */
public class GrandExchange
{
   private String query;
   private JsonArray items;

   /**
    * Constructs a GrandExchange object (up to 12 results)
    * @param q query to search for
    */
   public GrandExchange(String q)
   {
      query = q;

      try
      {
         // Build url with encoded query
         String urlString = "http://services.runescape.com/m=itemdb_oldschool/api/catalogue/items.json?category=1"
                 + "&alpha=" + URLEncoder.encode(query, "UTF-8") + "&page=1";
         URL url = new URL(urlString);

         // Open streams
         InputStream is = url.openStream();
         InputStreamReader isr = new InputStreamReader(is);
         BufferedReader br = new BufferedReader(isr);

         // Parse JSON
         JsonElement results = new JsonParser().parse(br);
         items = results.getAsJsonObject().get("items").getAsJsonArray();
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
   }

   public String getQuery()
   {
      return query;
   }

   public int getSize()
   {
      return items.size();
   }

   public String getIconUrl(int index)
   {
      return items.get(index).getAsJsonObject().get("icon").getAsString();
   }

   public String getIconLargeUrl(int index)
   {
      return items.get(index).getAsJsonObject().get("icon_large").getAsString();
   }

   public String getId(int index)
   {
      return items.get(index).getAsJsonObject().get("id").getAsString();
   }

   public String getType(int index)
   {
      return items.get(index).getAsJsonObject().get("type").getAsString();
   }

   public String getTypeIcon(int index)
   {
      return items.get(index).getAsJsonObject().get("typeIcon").getAsString();
   }

   public String getName(int index)
   {
      return items.get(index).getAsJsonObject().get("name").getAsString();
   }

   public String getDescription(int index)
   {
      return items.get(index).getAsJsonObject().get("description").getAsString();
   }

   public String getCurrentTrend(int index)
   {
      return items.get(index).getAsJsonObject().get("current").getAsJsonObject().get("trend").getAsString();
   }

   public String getCurrentPrice(int index)
   {
      return items.get(index).getAsJsonObject().get("current").getAsJsonObject().get("price").getAsString();
   }

   public String getTodaysTrend(int index)
   {
      return items.get(index).getAsJsonObject().get("today").getAsJsonObject().get("trend").getAsString();
   }

   public String getTodaysPrice(int index)
   {
      return items.get(index).getAsJsonObject().get("today").getAsJsonObject().get("price").getAsString();
   }

   public boolean isMembers(int index)
   {
      return items.get(index).getAsJsonObject().get("members").getAsBoolean();
   }

   public String toString()
   {
      return "Grand exchange query: " + getQuery();
   }
}
