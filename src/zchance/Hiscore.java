package zchance;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Pulls data from OSRS hiscores
 *
 * @author Zed Chance
 */
public class Hiscore
{
   private String username;
   private String[] hiscores;

   /**
    * Constructs a hiscore object
    * @param u username
    */
   public Hiscore(String u)
   {
      username = u;
      hiscores = new String[36];
      String urlString = "https://secure.runescape.com/m=hiscore_oldschool/index_lite.ws?player="
              + URLEncoder.encode(u);
      try
      {
         URL url = new URL(urlString);

         InputStream is = url.openStream();
         InputStreamReader isr = new InputStreamReader(is);
         BufferedReader br = new BufferedReader(isr);

         String hiscoreString = "";
         for (int i = 0; i < 35; i++)
         {
            hiscoreString += br.readLine() + "\n";
         }

         hiscores = hiscoreString.split(",|\n");
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
   }

   public String getUsername()
   {
      return username;
   }

   public String getOverallRank()
   {
      return hiscores[0];
   }

   public String getOverallLevel()
   {
      return hiscores[1];
   }

   public String getOverallXP()
   {
      return hiscores[2];
   }

   public String getAttackRank()
   {
      return hiscores[3];
   }

   public String getAttackLevel()
   {
      return hiscores[4];
   }

   public String getAttackXP()
   {
      return hiscores[5];
   }

   public String getDefenceRank()
   {
      return hiscores[6];
   }

   public String getDefenceLevel()
   {
      return hiscores[7];
   }

   public String getDefenceXP()
   {
      return hiscores[8];
   }

   public String getStrengthRank()
   {
      return hiscores[9];
   }

   public String getStrengthLevel()
   {
      return hiscores[10];
   }

   public String getStrengthXP()
   {
      return hiscores[11];
   }

   public String getHitpointsRank()
   {
      return hiscores[12];
   }

   public String getHitpointsLevel()
   {
      return hiscores[13];
   }

   public String getHitpointsXP()
   {
      return hiscores[14];
   }

   public String getRangedRank()
   {
      return hiscores[15];
   }

   public String getRangedLevel()
   {
      return hiscores[16];
   }

   public String getRangedXP()
   {
      return hiscores[17];
   }

   public String getPrayerRank()
   {
      return hiscores[18];
   }

   public String getPrayerLevel()
   {
      return hiscores[18];
   }

   public String getPrayerXP()
   {
      return hiscores[20];
   }

   public String getMagicRank()
   {
      return hiscores[21];
   }

   public String getMagicLevel()
   {
      return hiscores[22];
   }

   public String getMagicXP()
   {
      return hiscores[23];
   }

   public String getCookingRank()
   {
      return hiscores[24];
   }

   public String getCookingLevel()
   {
      return hiscores[25];
   }

   public String getCookingXP()
   {
      return hiscores[26];
   }

   public String getWoodcuttingRank()
   {
      return hiscores[27];
   }

   public String getWoodcuttingLevel()
   {
      return hiscores[28];
   }

   public String getWoodcuttingXP()
   {
      return hiscores[29];
   }

   public String getFletchingRank()
   {
      return hiscores[30];
   }

   public String getFletchingLevel()
   {
      return hiscores[31];
   }

   public String getFletchingXP()
   {
      return hiscores[32];
   }

   public String getFishingRank()
   {
      return hiscores[33];
   }

   public String getFishingLevel()
   {
      return hiscores[34];
   }

   public String getFishingXP()
   {
      return hiscores[35];
   }

   public String getFiremakingRank()
   {
      return hiscores[36];
   }

   public String getFiremakingLevel()
   {
      return hiscores[37];
   }

   public String getFiremakingXP()
   {
      return hiscores[38];
   }

   public String getCraftingRank()
   {
      return hiscores[39];
   }

   public String getCraftingLevel()
   {
      return hiscores[40];
   }

   public String getCraftingXP()
   {
      return hiscores[41];
   }

   public String getSmithingRank()
   {
      return hiscores[42];
   }

   public String getSmithingLevel()
   {
      return hiscores[43];
   }

   public String getSmithingXP()
   {
      return hiscores[44];
   }

   public String getMiningRank()
   {
      return hiscores[45];
   }

   public String getMiningLevel()
   {
      return hiscores[46];
   }

   public String getMiningXP()
   {
      return hiscores[47];
   }

   public String getHerbloreRank()
   {
      return hiscores[48];
   }

   public String getHerbloreLevel()
   {
      return hiscores[49];
   }

   public String getHerbloreXP()
   {
      return hiscores[50];
   }

   public String getAgilityRank()
   {
      return hiscores[51];
   }

   public String getAgilityLevel()
   {
      return hiscores[52];
   }

   public String getAgilityXP()
   {
      return hiscores[53];
   }

   public String getThievingRank()
   {
      return hiscores[54];
   }

   public String getThievingLevel()
   {
      return hiscores[55];
   }

   public String getThievingXP()
   {
      return hiscores[56];
   }

   public String getSlayerRank()
   {
      return hiscores[57];
   }

   public String getSlayerLevel()
   {
      return hiscores[58];
   }

   public String getSlayerXP()
   {
      return hiscores[59];
   }

   public String getFarmingRank()
   {
      return hiscores[60];
   }

   public String getFarmingLevel()
   {
      return hiscores[61];
   }

   public String getFarmingXP()
   {
      return hiscores[62];
   }

   public String getRunecraftRank()
   {
      return hiscores[63];
   }

   public String getRunecraftLevel()
   {
      return hiscores[64];
   }

   public String getRunecraftXP()
   {
      return hiscores[65];
   }

   public String getHunterRank()
   {
      return hiscores[66];
   }

   public String getHunterLevel()
   {
      return hiscores[67];
   }

   public String getHunterXP()
   {
      return hiscores[68];
   }

   public String getConstructionRank()
   {
      return hiscores[69];
   }

   public String getConstructionLevel()
   {
      return hiscores[70];
   }

   public String getConstructionXP()
   {
      return hiscores[71];
   }

   public String getBountyHunterHunterRank()
   {
      return hiscores[72];
   }

   public  String getBountyHunterHunterScore()
   {
      return hiscores[73];
   }

   public String getBountyHunterRogueRank()
   {
      return hiscores[74];
   }

   public String getBountyHunterRogueScore()
   {
      return hiscores[75];
   }

   public String getLMSRank()
   {
      return hiscores[76];
   }

   public String getLMSScore()
   {
      return hiscores[77];
   }

   public String getClueScrollsAllRank()
   {
      return hiscores[78];
   }

   public String getClueScrollsAllScore()
   {
      return hiscores[79];
   }

   public String getClueScrollsBeginnerRank()
   {
      return hiscores[80];
   }

   public String getClueScrollsBeginnerScore()
   {
      return hiscores[81];
   }

   public String getClueScrollsEasyRank()
   {
      return hiscores[82];
   }

   public String getClueScrollsEasyScore()
   {
      return hiscores[83];
   }

   public String getClueScrollsMediumRank()
   {
      return hiscores[84];
   }

   public String getClueScrollsMediumScore()
   {
      return hiscores[85];
   }

   public String getClueScrollsHardRank()
   {
      return hiscores[86];
   }

   public String getClueScrollsHardScore()
   {
      return hiscores[87];
   }

   public String getClueScrollsEliteRank()
   {
      return hiscores[88];
   }

   public String getClueScrollsEliteScore()
   {
      return hiscores[89];
   }

   public String getClueScrollsMasterRank()
   {
      return hiscores[90];
   }

   public String getClueScrollsMasterScore()
   {
      return hiscores[91];
   }

   public String toString()
   {
      return "Hiscores for " + getUsername();
   }
}
