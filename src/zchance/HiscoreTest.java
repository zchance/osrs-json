package zchance;

public class HiscoreTest
{
   public static void main(String[] args)
   {
      Hiscore bluetrane = new Hiscore("bluetrane");

      System.out.println(bluetrane);

      System.out.println("Overall rank: " + bluetrane.getOverallRank());
      System.out.println("Overall level: " + bluetrane.getOverallLevel());
      System.out.println("Overall XP: " + bluetrane.getOverallXP());
      System.out.println("Attack rank: " + bluetrane.getAttackRank());
      System.out.println("Attack level: " + bluetrane.getAttackLevel());
      System.out.println("Attack XP: " + bluetrane.getAttackXP());
      System.out.println("Firemaking XP: " + bluetrane.getFiremakingXP());
      System.out.println("Construction XP: " + bluetrane.getConstructionXP());
      System.out.println("LMS rank: " + bluetrane.getLMSRank());
      System.out.println("All clue scrolls: " + bluetrane.getClueScrollsAllScore());
      System.out.println("Medium clue scrolls: " + bluetrane.getClueScrollsMediumScore());
      System.out.println("Elite clue scrolls: " + bluetrane.getClueScrollsEliteScore());
   }
}
